<?php

/**
* Modelo ActiveRecord User
*/
namespace Mvc\Model;

require_once '../app/Model.php';

class Module extends \Mvc\App\Model
{
    const PAGE_SIZE = 3;

    public function __construct()
    {
        parent::__construct();
    }

    public function get($page)
    {
        $offset = ($page - 1) * $this::PAGE_SIZE;
        $size = $this::PAGE_SIZE;

        $sql = "SELECT * FROM module  limit $size offset $offset";
        $stmt = $this->_pdo->prepare($sql);
  //      $stmt->bindValue(':name', $_POST['name']);
        $stmt->execute();
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $rows;
    }

    public function pages()
    {
        $sql = "select count(id) AS pages FROM module";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute();
        $row =  $stmt->fetch(\PDO::FETCH_ASSOC);
        var_dump($row);
        return ceil($row['pages']/$this::PAGE_SIZE);
    }

    public function insert()
    {

        $sql = "insert into module(name, code, shortName) values(:name, :code, :shortName)";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->bindValue(':name', $_POST['name']);
        $stmt->bindValue(':code', $_POST['code']);
        $stmt->bindValue(':shortName', $_POST['shortName']);

        $stmt->execute();
    }
}
