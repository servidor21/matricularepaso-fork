<?php
    require '../views/header.php';
?>
<div id="content">
    <h1>Lista de modulos</h1>
    <p><a href="module/new">Nuevo</a></p>

    <table>
        <tr>
            <th>Codigo</th>
            <th>Abreviatura</th>
            <th>Nombre</th>
        </tr>

        <?php foreach ($rows as $row) : ?>
            <tr>
            <td><?php echo $row[code] ?></td>
            <td><?php echo $row[shortName] ?></td>
            <td><?php echo $row[name] ?></td>
            </tr>
        <?php endforeach ?>
    </table>
    <?php for ($i = 1; $i <= $pages; $i++) : ?>
        <a href="/module/index/<?php echo $i ?>"> <?php echo $i ?></a> - 
    <?php endfor ?>

</div>

<?php
    require '../views/footer.php';
?>
