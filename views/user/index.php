<?php
    require '../views/header.php';
?>
<div id="content">
    <h1>Lista de usuarios</h1>
    <p>
        <a href="/user/new">Alta usuario</a>
    </p>
    <table>
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Login</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($rows as $row) : ?>
        <tr>
            <td><?php echo $row['name'] ?></td>
            <td><?php echo $row['surname'] ?></td>
            <td><?php echo $row['login'] ?></td>
        </tr>            
        <?php endforeach ?>
        </tbody>

    </table>
    <p>
        <a href="/user/new">Alta usuario</a>
    </p>
</div>

<?php
    require '../views/footer.php';
?>
