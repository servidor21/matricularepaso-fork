<?php

/**
* Clase con información de ayuda
*/
namespace Mvc\Controller;

class Help
{
    public function __construct()
    {
        // echo "Estamos en Help";
    }

    public function index()
    {
        echo "Por necesidades de tiempo hacemos aquí el session_destroy";
        session_destroy();
        $_SESSION = null;

        require '../views/help/index.php';
    }
}
