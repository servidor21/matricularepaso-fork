<?php
/**
* Controlador de Module
*/
namespace Mvc\Controller;

require_once '../app/Controller.php';
require_once '../models/Module.php';


class Module extends \Mvc\App\Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->_model = new \Mvc\Model\Module;
    }

    public function index($page = 1)
    {
        $rows = $this->_model->get($page);
        $pages = $this->_model->pages();

        require('../views/module/index.php');

        $_SESSION['page'] = $page;
    }

    public function new()
    {
        require('../views/module/new.php');
    }

    public function insert()
    {
        $this->_model->insert();
        header('Location: /module');
        //require('../views/module/new.php');
    }
}
